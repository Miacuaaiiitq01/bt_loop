/*
    Bài 1: Tìm số nguyên dương lớn nhất.
*/
{
  var soNguyenDuong = document.getElementById("soNguyenDuong").value * 1;
  var sum = 0;
  for (var i = 0; sum < soNguyenDuong ; i++) {
    sum += i;
  }
  document.getElementById("kQuaBai1").innerHTML = i;
}
/*
    Bài 2: Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2 + x^3 + … + x^n
*/
{
  function tinhTong(x, n) {
    var a = 1;
    var b = 0;
    for (var i = 1; i <= n; i++) {
      a *= x;
      b += a;
    }
    return b;
  }
  document.getElementById("btnTinhBai2").onclick = function () {
    var x = Number(document.getElementById("x").value);
    var n = Number(document.getElementById("n").value);
    var kQuaBai2 = tinhTong(x, n);
    document.getElementById("kQuaBai2").innerHTML = kQuaBai2;
  };
}

/*
    Bài 3: Nhập vào n. Tính giai thừa 1*2*...n
*/
{
  function giaiThua(nhapSo) {
    var a = 1;
    for (var i = 1; i <= nhapSo; i++) {
      a *= i;
    }
    return a;
  }

  document.getElementById("btnTinhBai3").onclick = function () {
    var nhapSo = document.querySelector("#nhapSo").value * 1;

    var kQuaBai3 = giaiThua(nhapSo);
    document.getElementById("kQuaBai3").innerHTML = kQuaBai3;
  };
}

/*
    Bài 4: Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div.
            Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì
            background màu xanh.
*/

function inTheDiv(input) {
  var result = "";
  for (var dem = 1; dem <= input; dem++) {
    if (dem % 2 == 0) {
      var divChan = `<div class="alert alert-danger mt-2">Thẻ div chẵn: ${dem}</div>`;
      result += divChan;
    } else {
      var divLe = `<div class="alert alert-primary mt-2">Thẻ div lẻ: ${dem}</div>`;
      result += divLe;
    }
  }
  return result;
}
document.getElementById("btnIntheDiv").onclick = function () {
  var input = document.getElementById("iSo").value * 1;
  // output: String
  result = inTheDiv(input);
  document.getElementById("ketQua1").innerHTML = result;
};
